-- ******* 14
-- Per ogni data in `dates.csv` con codice `MN` trovare tra le aziende in `symbols.csv` quella con il valore totale minimo, ovvero il prodotto tra il prezzo ufficiale (`price`) e il numero di azioni (`volume`) in quella data.

-- Restituire: date, symbol, company_name, value

-- Esempio
--     date    | symbol  |          company_name           |  value
-- ------------+---------+---------------------------------+---------
--  2001-11-27 | AMCE    | American Claims Evaluation Inc. | 474.000
--  2001-12-21 | MBLA    | National Mercantile Bancorp     |   0.000
--  2001-12-21 | AMCE    | American Claims Evaluation Inc. |   0.000

\c db2016
SET search_path TO assignment3,public;

WITH getvalue AS (
  SELECT date, symbol, price*volume AS value
  FROM stock_price NATURAL JOIN symbols
  WHERE date IN (SELECT date FROM dates WHERE type='MN')
)

SELECT date, symbol, company_name, ROUND(value::DECIMAL, 3) AS value
FROM getvalue NATURAL JOIN stock_symbols
WHERE value IN (SELECT MIN(value) FROM getvalue GROUP BY date);
