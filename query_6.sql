-- ******* 06
-- Trovare i possibili match tra aziende in `companies.csv` e aziende in `stock_symbols.csv`.
-- Un match è valido solo se i primi 5 caratteri (spazi inclusi) di `company_name` in `stock_symbols.csv` appaiono in `name` di `companies.csv`, e al contempo se la differenza nel numero di caratteri tra `company_name` e `name` (ovvero lunghezza(`company_name`) -lunghezza(`name`)) è minore 11.
-- Per ogni match valido tornare il possibile `symbol` e lo stato.

-- Restituire: company_name, name, symbol, state

-- N.b. SELECT  left('stringa1 abcd',5) = left('stringa2 abcdefghi', 5) ;
-- http://www.postgresql.org/docs/9.3/static/functions-string.html

-- Esempio
--           company_name           |             name              | symbol  | state
-- ---------------------------------+-------------------------------+---------+-------
--  American Claims Evaluation Inc. | American Axle & Manufacturing | AMCE    | MI
--  Microsoft Corp.                 | Microsoft                     | MSFT    | WA

\c db2016
SET search_path TO assignment3,public;

SELECT company_name, name, symbol, state
FROM companies, stock_symbols
WHERE LEFT(company_name, 5) = LEFT(name, 5) AND CHAR_LENGTH(company_name) - CHAR_LENGTH(name) < 11;
