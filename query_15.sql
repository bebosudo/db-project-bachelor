-- ******* 15
-- Per il mercato scelto (in `market.csv`) restituire l'azienda con il periodo più corto di quotazione misurato in giorni, ovvero la differenza tra la prima e l'ultima data con cui appare in `stock_price.csv`.

-- Restituire: market, symbol, company_name, first_date, last_date, days

-- N.b. http://www.postgresql.org/docs/9.3/static/functions-datetime.html
-- SELECT date '2001-10-01' - date '2001-09-28';

-- Esempio
--  market | symbol  |          company_name           | first_date | last_date  | days
-- --------+---------+---------------------------------+------------+------------+------
--  NYSE   | KFS     | Kingsway Financial Services Inc | 2001-12-18 | 2001-12-26 |    8

\c db2016
SET search_path TO assignment3,public;

WITH getdays AS (
  SELECT symbol, MIN(date) AS first_date, MAX(date) AS last_date, MAX(date)-MIN(date) AS tempdays
  FROM stock_price NATURAL JOIN stock_symbols
  WHERE market = (SELECT market FROM market)
  GROUP BY symbol
)

SELECT SY.market, SY.symbol, SY.company_name, getdays.first_date, getdays.last_date, days
FROM    (SELECT MIN(tempdays) AS days FROM getdays) AS T2
        INNER JOIN getdays ON T2.days=getdays.tempdays
        INNER JOIN stock_symbols SY ON getdays.symbol=SY.symbol;
