\c db2016
SET search_path TO assignment3,public;

COPY companies FROM '/tmp/dati/companies.csv' DELIMITER ';' CSV NULL AS 'None';
COPY stock_symbols FROM '/tmp/dati/stock_symbols.csv' CSV FORCE NULL company_name NULL AS 'N/A';
COPY stock_price FROM '/tmp/dati/stock_price.csv' CSV;
COPY dates FROM '/tmp/dati/dates.csv' CSV;
COPY market FROM '/tmp/dati/market.csv' CSV;
COPY symbols FROM '/tmp/dati/symbols.csv' CSV;
